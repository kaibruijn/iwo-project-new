#!/bin/bash

echo 'The number of tweets by men:'

zless /net/corpora/twitter2/Tweets/2017/03/20170301\:*.out.gz | /net/corpora/twitter2/tools/tweet2tab user text | python get-gender.py | grep -v 'RT' | grep -v '@' | grep -w 'male' | wc -l

echo 'The number of tweets by women:'

zless /net/corpora/twitter2/Tweets/2017/03/20170301\:*.out.gz | /net/corpora/twitter2/tools/tweet2tab user text | python get-gender.py | grep -v 'RT' | grep -v '@' | grep -w 'female' | wc -l

echo 'The number of tweets by men talking about relationships:'

zless /net/corpora/twitter2/Tweets/2017/03/20170301\:*.out.gz | /net/corpora/twitter2/tools/tweet2tab user text | python get-gender.py | grep -v 'RT' | grep -v '@' | grep -w 'male' | grep -i -e 'liefde' -e 'relatie' -e 'vriendje' -e 'vriendinnetje' -e 'kroelen' -e 'knuffelen' -e 'schatje' -e 'lieverd' -e 'ik hou van je' -e 'liefje' | wc -l

echo 'The number of tweets by women talking about relationships:'

zless /net/corpora/twitter2/Tweets/2017/03/20170301\:*.out.gz | /net/corpora/twitter2/tools/tweet2tab user text | python get-gender.py | grep -v 'RT' | grep -v '@' | grep -w 'female' | grep -i -e 'liefde' -e 'relatie' -e 'vriendje' -e 'vriendinnetje' -e 'kroelen' -e 'knuffelen' -e 'schatje' -e 'lieverd' -e 'ik hou van je' -e 'liefje' | wc -l
